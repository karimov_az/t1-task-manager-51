package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.SessionDto;
import ru.t1.karimov.tm.dto.model.UserDto;

public interface IAuthService {

    @NotNull
    UserDto check(@Nullable String login, @Nullable String password) throws Exception;

    void invalidate(@Nullable SessionDto session) throws Exception;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    UserDto registry(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    SessionDto validateToken(@Nullable String token);

}
