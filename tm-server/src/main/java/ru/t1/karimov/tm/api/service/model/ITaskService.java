package ru.t1.karimov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    Task changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws Exception;

    @NotNull
    Task updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    Task updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
