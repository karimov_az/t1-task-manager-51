package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.karimov.tm.dto.response.task.TaskBindToProjectResponse;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @Override
    public void execute() throws Exception {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        @NotNull final TaskBindToProjectResponse response = getTaskEndpoint().bindTaskToProject(request);
        @Nullable final TaskDto task = response.getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public  String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
